package com.hugoleal.kidstaskmanager.ui.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.EffectiveTaskDatabase;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Kid;
import com.hugoleal.kidstaskmanager.ui.DailyTasksActivityHelper;
import com.hugoleal.kidstaskmanager.ui.WrongOperationDialogFragment;
import com.hugoleal.kidstaskmanager.ui.adapter.EffectiveTaskListAdapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class DailyTasksActivity extends AppCompatActivity implements RatingBar.OnRatingBarChangeListener {

    private static final String ACTIVITY_TITLE = "Today's tasks";
    private DailyTasksActivityHelper dailyTasksActivityHelper;
    private EffectiveTaskDAO effectiveTaskDAO;
    private EffectiveTaskListAdapter effectiveTasksListAdapter;
    private LocalDate today = LocalDate.now();
    private LocalDate date = today;
    private EffectiveTask choiceTask;
    private Kid kid;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_tasks);
        setTitle(ACTIVITY_TITLE);
        setKidName();
        setKid();
        buttonsConfigure();
        listConfigure();
        effectiveTaskDAOConfigure();
        dailyTasksActivityHelper = new DailyTasksActivityHelper(this, effectiveTasksListAdapter,
                effectiveTaskDAO, kid.getId());
    }

    private void setKid() {
        kid = (Kid) getIntent().getSerializableExtra("kid");
    }

    private void buttonsConfigure() {
        addButtonConfigure();
        backButtonConfigure();
        dashboardButtonConfigure();
    }

    @Override
    public void onResume() {
        super.onResume();
        dateConfigure(date);
        dailyTasksActivityHelper.updateList(date, today);
    }

    private void dateConfigure(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getString(R.string.dateFormat));
        Button dateButton = findViewById(R.id.activity_daily_tasks_date);
        dateButton.setText(date.format(formatter));
        dateButton.setOnClickListener(v -> showDatePickerDialog());
    }

    public void showDatePickerDialog() {

        DatePickerDialog datePickerDialog = new DatePickerDialog(this);
        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                date = LocalDate.of(year, month+1, dayOfMonth);
                onResume();
            }
        });
        datePickerDialog.show();

    }

    private void setKidName() {
        Kid kid = (Kid) getIntent().getSerializableExtra("kid");
        TextView kidsName = findViewById(R.id.activity_daily_tasks_kids_name);
        kidsName.setText(kid.getName());
    }

    private void listConfigure() {
        ListView tasksList = findViewById(R.id.activity_daily_tasks_list);
        effectiveTasksListAdapter = new EffectiveTaskListAdapter(this);
        tasksList.setAdapter(effectiveTasksListAdapter);

        configureOnItemClickListener(tasksList);

        registerForContextMenu(tasksList);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_daily_tasks_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.activity_daily_tasks_menu_remove) {
            dailyTasksActivityHelper.removeTask(item);
        }
        return super.onContextItemSelected(item);
    }

    private void effectiveTaskDAOConfigure() {
        effectiveTaskDAO = EffectiveTaskDatabase.getInstance(this)
                .getEffectiveTaskDAO();
    }

    private void configureOnItemClickListener(ListView taskList) {

        taskList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (!(date.compareTo(today) > 0)) {
                    choiceTask = (EffectiveTask) parent.getItemAtPosition(position);
                    Intent goToTaskDoneActivity = new Intent(DailyTasksActivity.this, RatingActivity.class);
                    goToTaskDoneActivity.putExtra("task", choiceTask);
                    startActivity(goToTaskDoneActivity);
                    //onResume();
                } else {
                    WrongOperationDialogFragment alert = new WrongOperationDialogFragment();
                    alert.show(getSupportFragmentManager(), "You can't rate a future task");
                }
            }
        });
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        System.out.println("on rate changed");
        choiceTask.setRate((int) rating);
        effectiveTaskDAO.save(choiceTask);
        onResume();
    }

    private void addButtonConfigure() {
        Button addButton = findViewById(R.id.activity_daily_tasks_add_task);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!(date.compareTo(today) < 0)) {
                    Intent goToTasksListActivity = new Intent(DailyTasksActivity.this, TasksListActivity.class);
                    goToTasksListActivity.putExtra("date", date);
                    goToTasksListActivity.putExtra("kidId", kid.getId());
                    startActivity(goToTasksListActivity);
                } else {
                    WrongOperationDialogFragment alert = new WrongOperationDialogFragment();
                    alert.show(getSupportFragmentManager(), "You can't add a task to do in the past");
                }
            }
        });
    }

    private void dashboardButtonConfigure() {
        Button addButton = findViewById(R.id.activity_daily_tasks_dashboard);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent goToDashboardActivity = new Intent(DailyTasksActivity.this, DashboardActivity.class);
                goToDashboardActivity.putExtra("kid", kid);
                startActivity(goToDashboardActivity);

            }
        });
    }

    private void backButtonConfigure() {
        Button backButton = findViewById(R.id.activity_daily_tasks_back);
        backButton.setOnClickListener(v -> finish());
    }

    /* @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data.hasExtra("date")) {
            date = (LocalDate) getIntent().getSerializableExtra("date");
            System.out.println(date.toString());
        }
    }*/

     /* @Override
    public void onDialogPositiveClick(DialogFragment dialog) {
        System.out.println("entrou no on positive click");
    }

    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        date = LocalDate.of(year, monthOfYear, dayOfMonth);
        System.out.println(date.toString());
        onResume();
    }*/

    /*newFragment = new DatePickerFragment(DailyTasksActivity.this);
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                System.out.println("ondataset do daily ac");
                date = LocalDate.of(year, month, dayOfMonth);
                onResume();
            }
        };
        newFragment.show(getSupportFragmentManager(), "DatePickerFragment");
         */
}