package com.hugoleal.kidstaskmanager.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TasksListAdapter extends BaseAdapter {

    private final List<Task> tasks = new ArrayList<>();
    private final Context context;

    public TasksListAdapter (Context context){
        this.context = context;
    }


    @Override
    public int getCount() {
        return tasks.size();
    }

    @Override
    public Task getItem(int position) {
        return tasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return tasks.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View newView = LayoutInflater.from(context).inflate(R.layout.item_task_new, parent, false);

        Task task = tasks.get(position);
        TextView taskName = newView.findViewById(R.id.item_task_name);
        taskName.setText(task.getName());

        TextView taskFrequency = newView.findViewById(R.id.item_task_frequency);
        taskFrequency.setText(task.getFrequency().toString());

        return newView;
    }

    public void remove(Task task) {
        tasks.remove(task);
        notifyDataSetChanged();
    }

    public void clear() {
        tasks.clear();
    }

    public void addAll(List<Task> tasks) {
        this.tasks.addAll(tasks);
        notifyDataSetChanged();
    }
}
