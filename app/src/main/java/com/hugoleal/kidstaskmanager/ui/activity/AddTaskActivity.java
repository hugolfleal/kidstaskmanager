package com.hugoleal.kidstaskmanager.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.TasksDatabase;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.Frequency;
import com.hugoleal.kidstaskmanager.model.Importance;
import com.hugoleal.kidstaskmanager.model.Task;
import com.hugoleal.kidstaskmanager.ui.WrongOperationDialogFragment;

import java.time.LocalDate;

public class AddTaskActivity extends AppCompatActivity {

    private static final String TITLE_NEW = "Add a new Task";
    private static final String TITLE_EDIT = "Edit the Task";
    private EditText fieldName;
    private RadioGroup frequencyRadioGroup;
    private RadioGroup importanceRadioGroup;
    private TaskDAO taskDAO;
    private Task task;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_task);

        TasksDatabase database = TasksDatabase.getInstance(this);
        taskDAO = database.getTaskDAO();

        initializeFields();

        loadTask();

        configureSaveButton();

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void loadTask() {
        Intent tasksData = getIntent();

        if (tasksData.hasExtra("task")) {
            setTitle(TITLE_EDIT);
            task = (Task) tasksData.getSerializableExtra("task");
            fieldName.setText(task.getName());
            frequencyRadioGroup.check(task.getFrequency().getButtonId());
            importanceRadioGroup.check(Importance.getButtonId(task.getImportance()));
        } else {
            setTitle(TITLE_NEW);
            LocalDate date = (LocalDate) getIntent().getSerializableExtra("date");
            int kidId = (int) getIntent().getSerializableExtra("kidId");
            task = new Task(date, kidId);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void configureSaveButton() {
        Button saveButton = findViewById(R.id.activity_add_task_saveButton);
        saveButton.setOnClickListener(v -> {

            RadioButton frequencyRadioButton = findViewById(frequencyRadioGroup.getCheckedRadioButtonId());
            RadioButton importanceRadioButton = findViewById(importanceRadioGroup.getCheckedRadioButtonId());

            if (anyButtonIsChecked(frequencyRadioButton, importanceRadioButton)) {

                settingTask(frequencyRadioButton, importanceRadioButton);

                if (task.hasId()) {
                    taskDAO.edit(task);
                } else {
                    taskDAO.save(task);
                }
                finish();
            } else {
                WrongOperationDialogFragment alert = new WrongOperationDialogFragment();
                alert.show(getSupportFragmentManager(), "You must choose task frequency and importance");
            }

        });
    }

    private void initializeFields() {
        fieldName = findViewById(R.id.activity_add_task_name);
        frequencyRadioGroup = findViewById(R.id.activity_add_task_radioGroup);
        importanceRadioGroup = findViewById(R.id.activity_add_task_importance_radioGroup);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void settingTask(RadioButton frequencyRadioButton, RadioButton importanceRadioButton) {
        String name = fieldName.getText().toString();

        Frequency frequency = Frequency.valueOf(frequencyRadioButton.getText().toString().toUpperCase());

        String importanceResult = importanceRadioButton.getText().toString().toUpperCase();
        Importance importance = Importance.valueOf(importanceResult);
        int maxScore = importance.getImp();

        task.setFrequency(frequency);
        task.setImportance(maxScore);
        task.setName(name);
    }

    private boolean anyButtonIsChecked(RadioButton frequencyRadioButton, RadioButton importanceRadioButton) {
        return frequencyRadioButton != null && importanceRadioButton != null;
    }
}