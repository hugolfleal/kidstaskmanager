package com.hugoleal.kidstaskmanager.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.AdapterView;

import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Task;
import com.hugoleal.kidstaskmanager.ui.activity.AddTaskActivity;
import com.hugoleal.kidstaskmanager.ui.adapter.TasksListAdapter;

import java.util.List;

public class TasksListActivityHelper {

    private final Context context;
    private final TasksListAdapter listAdapter;
    private final TaskDAO taskDAO;

    public TasksListActivityHelper(Context context, TasksListAdapter listAdapter, TaskDAO taskDAO) {
        this.context = context;
        this.listAdapter = listAdapter;
        this.taskDAO = taskDAO;
    }

    public void removeTask(MenuItem item){
        new AlertDialog.Builder(context)
                .setTitle("Removing Task")
                .setMessage("Are you sure you want to remove?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Task choiceTask = getTask(item);
                        listAdapter.remove(choiceTask);
                        taskDAO.remove(choiceTask);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void removeTaskWithEffectiveTask(MenuItem item, EffectiveTaskDAO effectiveTaskDAO) {
        new AlertDialog.Builder(context)
                .setTitle("Removing Task with Effective Task associated")
                .setMessage("Are you sure you want to remove? This action will remove associated Effective Tasks too")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Task choiceTask = getTask(item);
                        int taskId = choiceTask.getId();
                        List<EffectiveTask> effectiveTasks = effectiveTaskDAO.getEffectiveTaskByTaskId(taskId);
                        for (EffectiveTask et: effectiveTasks) {
                            effectiveTaskDAO.remove(et);
                        }

                        listAdapter.remove(choiceTask);
                        taskDAO.remove(choiceTask);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void editTask(MenuItem item){
        Task choiceTask = getTask(item);
        Intent goToAddTaskActivity = new Intent(context, AddTaskActivity.class);
        goToAddTaskActivity.putExtra("task", choiceTask);
        context.startActivity(goToAddTaskActivity);

    }

    public int getTaskId(MenuItem item) {
        Task choiceTask = getTask(item);
        return choiceTask.getId();
    }

    private Task getTask(MenuItem item) {
        AdapterView.AdapterContextMenuInfo infoMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        return listAdapter.getItem(infoMenu.position);
    }

}
