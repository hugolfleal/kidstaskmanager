package com.hugoleal.kidstaskmanager.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.hugoleal.kidstaskmanager.ui.activity.DailyTasksActivity;

import java.time.LocalDate;
import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private DatePickerDialog datePickerDialog;
    private DailyTasksActivity dailyTasksActivity;

    public DatePickerFragment(DailyTasksActivity dailyTasksActivity) {
        this.dailyTasksActivity = dailyTasksActivity;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LocalDate today = LocalDate.now();
        int year = today.getYear();
        int month = today.getMonthValue();
        int day = today.getDayOfMonth();

        datePickerDialog = new DatePickerDialog(getActivity(), this, year, month -1 , day);
        return datePickerDialog;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void onDateSet(DatePicker view, int year, int month, int day) {
        System.out.println("ondataset do date picker");
        dailyTasksActivity.setDate(LocalDate.of(year, month +1, day));
        dailyTasksActivity.onResume();
    }

    public DatePickerDialog getDatePickerDialog() {
        return datePickerDialog;
    }

    public void setDatePickerDialog(DatePickerDialog datePickerDialog) {
        this.datePickerDialog = datePickerDialog;
    }

    /*public DatePickerDialog getDatePickerDialog() {
        return datePickerDialog;
    }

     public interface NoticeDialogListener {
        void onDialogPositiveClick(DialogFragment dialog);
    }

    NoticeDialogListener listener;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);

        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(" must implement NoticeDialogListener");
        }
    }*/

}
// TODO: 02/04/2021 excluir essa classe