package com.hugoleal.kidstaskmanager.ui.adapter;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.EffectiveTaskDatabase;
import com.hugoleal.kidstaskmanager.database.TasksDatabase;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.DashboardItem;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Kid;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DashboardListAdapter extends BaseAdapter {

    private final Context context;
    private List<DashboardItem> dashboardItemList = new ArrayList<>();
    private Kid kid;
    private final int maxRate = 5;

    public DashboardListAdapter(Context context, Kid kid) {
        this.context = context;
        this.kid = kid;
    }

    @Override
    public int getCount() {
        return dashboardItemList.size();
    }

    @Override
    public DashboardItem getItem(int position) {
        return dashboardItemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View newView = LayoutInflater.from(context).inflate(R.layout.item_dashboard, parent, false);

        TextView dashboardItemDate = newView.findViewById(R.id.item_dashboard_date);
        LocalDate localDate = dashboardItemList.get(position).getDate();
        dashboardItemDate.setText(localDate.toString());

        ProgressBar progressBar = newView.findViewById(R.id.item_dashboard_progressbar);
        int progressBarProgress = (int) dashboardItemList.get(position).getDayRate();
        int minKidRate = 100 * kid.getGrade() / 20;
        if (progressBarProgress < minKidRate){
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.RED));
        }
        progressBar.setProgress(progressBarProgress);

        return newView;
    }

    public void clear() {
        dashboardItemList.clear();
    }

    public void addAll(List<DashboardItem> dashboardItemList) {
        this.dashboardItemList.addAll(dashboardItemList);
        notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void updateList(LocalDate day, LocalDate lastDay) {
        clear();
        EffectiveTaskDAO effectiveTaskDAO = EffectiveTaskDatabase.getInstance(context).getEffectiveTaskDAO();
        TaskDAO taskDAO = TasksDatabase.getInstance(context).getTaskDAO();
        while (day.compareTo(lastDay) <= 0) {

            List<EffectiveTask> effectiveTaskList = effectiveTaskDAO.getByIdAndDate(day, kid.getId());

            if (effectiveTaskList.size() > 0) {
                int rate = 0;
                int maxGeralRate = 0;
                int importance;
                for (EffectiveTask et : effectiveTaskList) {
                    importance = taskDAO.getImportanceBYTaskAndKidId(kid.getId(), et.getTaskId());
                    rate += et.getRate() * importance;
                    maxGeralRate += importance * maxRate;

                }

                createNewDashboardItem(day, rate, maxGeralRate);

            }
            day = day.plusDays(1);
        }
        notifyDataSetChanged();
    }

    private void createNewDashboardItem(LocalDate day, int rate, int maxValue) {
        double finalRate = 100 * rate / maxValue;
        DashboardItem newDashBoardItem = new DashboardItem(day, finalRate);
        dashboardItemList.add(newDashBoardItem);
    }
}
