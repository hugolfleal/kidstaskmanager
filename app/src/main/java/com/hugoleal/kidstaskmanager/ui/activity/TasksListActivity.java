package com.hugoleal.kidstaskmanager.ui.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.EffectiveTaskDatabase;
import com.hugoleal.kidstaskmanager.database.TasksDatabase;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Task;
import com.hugoleal.kidstaskmanager.ui.TasksListActivityHelper;
import com.hugoleal.kidstaskmanager.ui.adapter.TasksListAdapter;

import java.time.LocalDate;
import java.util.List;

public class TasksListActivity extends AppCompatActivity {

    private static final String ACTIVITY_TITLE = "Choose or add a task";
    private LocalDate date;
    private TasksListAdapter tasksListAdapter;
    private TasksListActivityHelper tasksListActivityHelper;
    private TaskDAO taskDAO;
    private EffectiveTaskDAO effectiveTaskDAO;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_list);
        setTitle(ACTIVITY_TITLE);

        dateConfigure();
        addButtonConfigure();
        backButtonConfigure();
        listConfigure();
        daosConfigure();
        tasksListActivityHelper = new TasksListActivityHelper(this, tasksListAdapter, taskDAO);
    }

    private void backButtonConfigure() {
        Button backButton = findViewById(R.id.activity_tasks_list_back);
        backButton.setOnClickListener(v -> finish());
    }

    @Override
    public void onResume() {
        super.onResume();
        tasksListAdapter.clear();
        tasksListAdapter.addAll(taskDAO.getByKidId((int) getIntent().getSerializableExtra("kidId")));
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void dateConfigure() {
        date = (LocalDate) getIntent().getSerializableExtra("date");
    }

    private void addButtonConfigure() {
        Button addButton = findViewById(R.id.activity_tasks_list_add_task);
        addButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                int kidId = (int) getIntent().getSerializableExtra("kidId");
                Intent goToAddTaskActivity = new Intent(TasksListActivity.this, AddTaskActivity.class);
                goToAddTaskActivity.putExtra("date", date);
                goToAddTaskActivity.putExtra("kidId", kidId);
                startActivity(goToAddTaskActivity);

            }
        });
    }

    private void listConfigure() {
        ListView tasksList = findViewById(R.id.activity_tasks_list_real_list);
        tasksListAdapter = new TasksListAdapter(this);
        tasksList.setAdapter(tasksListAdapter);

        configureOnItemClickListener(tasksList);

        registerForContextMenu(tasksList);
    }

    private void configureOnItemClickListener(ListView taskList) {

        taskList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int kidId = (int) getIntent().getSerializableExtra("kidId");
                Task choiceTask = (Task) parent.getItemAtPosition(position);
                EffectiveTask effectiveTask = new EffectiveTask(kidId, date, choiceTask.getId());
                effectiveTaskDAO.save(effectiveTask);
                finish();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_tasks_list_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.activity_tasks_list_menu_remove) {
            if (hasEffectiveTasks(item)){
                tasksListActivityHelper.removeTaskWithEffectiveTask(item, effectiveTaskDAO);
            } else {
                tasksListActivityHelper.removeTask(item);
            }
        }
        if (itemId == R.id.activity_tasks_list_menu_edit) {
            tasksListActivityHelper.editTask(item);
        }

        return super.onContextItemSelected(item);
    }

    private boolean hasEffectiveTasks(MenuItem item) {
        int taskId = tasksListActivityHelper.getTaskId(item);
        List<EffectiveTask> effectiveTasks = effectiveTaskDAO.getEffectiveTaskByTaskId(taskId);
        return effectiveTasks.size() > 0;
    }

    private void daosConfigure() {
        taskDAO = TasksDatabase.getInstance(this)
                .getTaskDAO();
        effectiveTaskDAO = EffectiveTaskDatabase.getInstance(this)
                .getEffectiveTaskDAO();
    }
}
