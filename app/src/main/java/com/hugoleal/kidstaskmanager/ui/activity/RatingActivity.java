package com.hugoleal.kidstaskmanager.ui.activity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;

import androidx.appcompat.app.AppCompatActivity;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.EffectiveTaskDatabase;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;

public class RatingActivity extends AppCompatActivity {

    private static final String ACTIVITY_TITLE = "Rate this task";
    private RatingBar ratingBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);
        setTitle(ACTIVITY_TITLE);
        buttonConfigure();
        //listConfigure();
        //taskDAOConfigure();
        //dailyTasksActivityHelper = new DailyTasksActivityHelper(this, tasksListAdapter, taskDAO);
    }

    private void buttonConfigure() {
        Button button = findViewById(R.id.activity_rating_submit_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ratingBar = findViewById(R.id.ratingBar);
                EffectiveTask effectiveTask = (EffectiveTask) getIntent().getSerializableExtra("task");
                EffectiveTaskDAO effectiveTaskDAO = EffectiveTaskDatabase.getInstance(RatingActivity.this.getParent()).getEffectiveTaskDAO();
                effectiveTask.setRate((int) ratingBar.getRating());
                effectiveTaskDAO.edit(effectiveTask);

                //RatingBar taskViewRatingBar = findViewById(R.id.item_task_done_ratingBar);
                //taskViewRatingBar.setRating(ratingBar.getRating());*/

                finish();
            }
        });
    }
}
