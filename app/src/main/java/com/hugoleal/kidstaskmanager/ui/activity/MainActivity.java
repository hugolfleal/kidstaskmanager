package com.hugoleal.kidstaskmanager.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.KidsDatabase;
import com.hugoleal.kidstaskmanager.database.dao.KidDAO;
import com.hugoleal.kidstaskmanager.model.Kid;
import com.hugoleal.kidstaskmanager.ui.MainActivityHelper;
import com.hugoleal.kidstaskmanager.ui.adapter.KidsListAdapter;


public class MainActivity extends AppCompatActivity {

    public static final String TITLE = "Kids Task Manager";
    private KidDAO kidDAO;
    private KidsListAdapter listAdapter;
    private MainActivityHelper mainActivityHelper;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setTitle(TITLE);
        buttonConfigure();
        listConfigure();
        kidDAOConfigure();
        setSubTitle();
        mainActivityHelper  = new MainActivityHelper(this, listAdapter, kidDAO);
    }

    private void setSubTitle() {
        TextView subTitle = findViewById(R.id.activity_main_title);
        if (kidDAO.all().isEmpty()){
            subTitle.setText("ADD A KID TO START");
        } else{
            subTitle.setText("CHOOSE THE KID");
        }

    }

    private void kidDAOConfigure() {
        kidDAO = KidsDatabase.getInstance(this)
                .getRoomKidDAO();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        int itemId = item.getItemId();
        if (itemId == R.id.activity_main_menu_remove) {
            mainActivityHelper.removeConfirm(item);
        }
        if ( itemId == R.id.activity_main_menu_edit){
            mainActivityHelper.editKid(item);
        }

        return super.onContextItemSelected(item);
    }


    private void buttonConfigure() {
        FloatingActionButton addButton = findViewById(R.id.activity_main_add_kid);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, AddKidActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        listAdapter.clear();
        listAdapter.addAll(kidDAO.all());
    }

    private void listConfigure() {
        ListView kidsList = findViewById(R.id.activity_main_kids_list);

        listAdapter = new KidsListAdapter(this);
        kidsList.setAdapter(listAdapter);

        configureOnItemClickListener(kidsList);
        //configureOnItemLongCLickListener(kidsList);
        registerForContextMenu(kidsList);
    }


    private void configureOnItemClickListener(ListView kidsList) {
        kidsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Kid choiceKid = (Kid) parent.getItemAtPosition(position);
                Intent goToDailyTasksActivity = new Intent(MainActivity.this, DailyTasksActivity.class);
                goToDailyTasksActivity.putExtra("kid", choiceKid);
                startActivity(goToDailyTasksActivity);

            }
        });
    }

     /* private void configureOnItemLongCLickListener(ListView kidsList) {
        kidsList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Kid choiceKid = (Kid) parent.getItemAtPosition(position);
                removeKid(choiceKid);
                return false;
            }
        });
    }*/

}
