package com.hugoleal.kidstaskmanager.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.view.MenuItem;
import android.widget.AdapterView;

import androidx.annotation.RequiresApi;

import com.hugoleal.kidstaskmanager.database.TasksDatabase;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Task;
import com.hugoleal.kidstaskmanager.ui.adapter.EffectiveTaskListAdapter;

import java.time.LocalDate;
import java.util.List;

public class DailyTasksActivityHelper {

    private final Context context;
    private final EffectiveTaskListAdapter effectiveTasksListAdapter;
    private final EffectiveTaskDAO effectiveTaskDAO;
    private TaskDAO taskDAO;
    private int kidId;

    public DailyTasksActivityHelper(Context context, EffectiveTaskListAdapter effectiveTasksListAdapter,
                                    EffectiveTaskDAO effectiveTaskDAO, int kidId) {
        this.context = context;
        this.effectiveTasksListAdapter = effectiveTasksListAdapter;
        this.effectiveTaskDAO = effectiveTaskDAO;
        this.kidId = kidId;
        taskDAO = TasksDatabase.getInstance(context).getTaskDAO();
    }

    public void removeTask(MenuItem item) {
        new AlertDialog.Builder(context)
                .setTitle("Removing Task from today")
                .setMessage("Are you sure you want to remove?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AdapterView.AdapterContextMenuInfo infoMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                        EffectiveTask choiceTask = effectiveTasksListAdapter.getItem(infoMenu.position);
                        effectiveTasksListAdapter.remove(choiceTask);
                        effectiveTaskDAO.remove(choiceTask);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void updateList(LocalDate date, LocalDate today) {
        effectiveTasksListAdapter.clear();
        List<EffectiveTask> effectiveTasks = effectiveTaskDAO.getByIdAndDate(date, kidId);
        List<Task> tasks = taskDAO.getByFrequency(date.getDayOfWeek().toString(), date.getDayOfMonth(), kidId);

        if (date.compareTo(today) < 0) {
            effectiveTasksListAdapter.addAll(effectiveTasks);
        } else if (date.compareTo(today) == 0) {
            for (Task task : tasks) {
                if (effectiveTasks.size() == 0) {
                    createEffectiveTask(date, task);
                    continue;
                } else {
                    for (int i = 0; i < effectiveTasks.size(); i++) {
                        if (effectiveTasks.get(i).getTaskId() == task.getId()) {
                            break;
                        }
                        if (i == effectiveTasks.size() - 1) {
                            createEffectiveTask(date, task);
                        }
                    }
                }
            }
            effectiveTasksListAdapter.addAll(effectiveTaskDAO.getByIdAndDate(date, kidId));
        } else {
            if (effectiveTasks.size() == 0) {
                addAllTasksTemporarily(date, effectiveTasks, tasks);
            } else {
                addUnaddedTasksTemporarily(date, effectiveTasks, tasks);
            }
            effectiveTasksListAdapter.addAll(effectiveTasks);
        }
    }

    private void addUnaddedTasksTemporarily(LocalDate date, List<EffectiveTask> effectiveTasks, List<Task> tasks) {
        for (Task task : tasks) {
            for (int i = 0; i < effectiveTasks.size(); i++) {
                if (effectiveTasks.get(i).getTaskId() == task.getId()) {
                    break;
                }
                if (i == effectiveTasks.size() - 1) {
                    EffectiveTask newEffectiveTask = new EffectiveTask(kidId, date, task.getId());
                    effectiveTasks.add(newEffectiveTask);
                }
            }
        }
    }

    private void addAllTasksTemporarily(LocalDate date, List<EffectiveTask> effectiveTasks, List<Task> tasks) {
        for (Task task : tasks) {
            EffectiveTask newEffectiveTask = new EffectiveTask(kidId, date, task.getId());
            effectiveTasks.add(newEffectiveTask);
        }
    }


    private void createEffectiveTask(LocalDate date, Task task) {
        EffectiveTask newEffectiveTask = new EffectiveTask(kidId, date, task.getId());
        effectiveTaskDAO.save(newEffectiveTask);
    }
}
