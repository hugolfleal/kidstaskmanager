package com.hugoleal.kidstaskmanager.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.model.Kid;

import java.util.ArrayList;
import java.util.List;

public class KidsListAdapter extends BaseAdapter {
    private final List<Kid> kids = new ArrayList<>();
    private final Context context;

    public KidsListAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return kids.size();
    }

    @Override
    public Kid getItem(int position) {
        return kids.get(position);
    }

    @Override
    public long getItemId(int position) {
        return kids.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View createdView = createView(parent);
        Kid kid = kids.get(position);
        viewToKid(createdView, kid);
        return createdView;
    }

    private void viewToKid(View createdView, Kid kid) {
        TextView name = createdView.findViewById(R.id.item_kid_name);
        name.setText(kid.getName());
        TextView age = createdView.findViewById(R.id.item_kid_age);
        age.setText(Integer.toString(kid.getAge()));
    }

    private View createView(ViewGroup parent) {
        return LayoutInflater
                .from(context)
                .inflate(R.layout.item_kid, parent, false);
    }

    public void clear() {
        kids.clear();
    }

    public void addAll(List<Kid> kids) {
        this.kids.addAll(kids);
        notifyDataSetChanged();
    }

    public void remove(Kid kid) {
        kids.remove(kid);
        notifyDataSetChanged();
    }
}
