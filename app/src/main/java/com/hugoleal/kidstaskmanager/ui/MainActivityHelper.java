package com.hugoleal.kidstaskmanager.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.hugoleal.kidstaskmanager.database.dao.KidDAO;
import com.hugoleal.kidstaskmanager.model.Kid;
import com.hugoleal.kidstaskmanager.ui.activity.AddKidActivity;
import com.hugoleal.kidstaskmanager.ui.adapter.KidsListAdapter;

public class MainActivityHelper {

    private final Context context;
    private final KidsListAdapter listAdapter;
    private final KidDAO kidDAO;

    public MainActivityHelper(Context context, KidsListAdapter listAdapter, KidDAO kidDAO) {
        this.context = context;
        this.listAdapter = listAdapter;
        this.kidDAO = kidDAO;

    }

    public void removeConfirm(@NonNull final MenuItem item) {
        new AlertDialog.Builder(context)
                .setTitle("Removing Kid")
                .setMessage("Are you sure you want to remove?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        AdapterView.AdapterContextMenuInfo infoMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
                        Kid choiceKid = listAdapter.getItem(infoMenu.position);
                        removeKid(choiceKid);
                    }
                })
                .setNegativeButton("NO", null)
                .show();
    }

    public void removeKid(Kid choiceKid) {
        kidDAO.remove(choiceKid);
        listAdapter.remove(choiceKid);
        Toast.makeText(context, "You removed " + choiceKid.getName(), Toast.LENGTH_LONG).show();
    }

    public void editKid(MenuItem item) {
        AdapterView.AdapterContextMenuInfo infoMenu = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Kid choiceKid = listAdapter.getItem(infoMenu.position); //(Kid) parent.getItemAtPosition(position);
        Intent goToAddKidActivity = new Intent(context, AddKidActivity.class);
        goToAddKidActivity.putExtra("kid", choiceKid);
        context.startActivity(goToAddKidActivity);
    }
}
