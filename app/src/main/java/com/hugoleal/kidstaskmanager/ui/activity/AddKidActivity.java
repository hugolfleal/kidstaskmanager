package com.hugoleal.kidstaskmanager.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.KidsDatabase;
import com.hugoleal.kidstaskmanager.database.dao.KidDAO;
import com.hugoleal.kidstaskmanager.model.Kid;

public class AddKidActivity extends AppCompatActivity {

    private static final String TITLE_NEW = "Add a new kid";
    private static final String TITLE_EDIT = "Edit the kid";
    private EditText fieldName;
    private EditText fieldAge;
    private EditText fieldGrade;
    private KidDAO kidDAO;
    private Kid kid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_kid);

        KidsDatabase database = KidsDatabase.getInstance(this);
        kidDAO = database.getRoomKidDAO();

        initializeFields();

        configureSaveButton();

        loadKid();

    }

    private void loadKid() {
        Intent kidsData = getIntent();

        if (kidsData.hasExtra("kid")){
            setTitle(TITLE_EDIT);
            kid = (Kid) kidsData.getSerializableExtra("kid");
            fieldName.setText(kid.getName());
            fieldAge.setText(Integer.toString(kid.getAge()));
            fieldGrade.setText(Integer.toString(kid.getGrade()));
        } else{
            setTitle(TITLE_NEW);
            kid = new Kid();
        }
    }

    private void configureSaveButton() {
        Button saveButton = findViewById(R.id.activity_add_kid_saveButton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                settingKid();

                if (kid.hasId()){
                    kidDAO.edit(kid);
                } else{
                    kidDAO.save(kid);
                }
                finish();
            }
        });
    }

    private void initializeFields() {
        fieldName = findViewById(R.id.activity_add_kid_name);
        fieldAge = findViewById(R.id.activity_add_kid_age);
        fieldGrade = findViewById(R.id.activity_add_kid_minimum_grade);
    }


    private void settingKid() {
        String name = fieldName.getText().toString();
        int age = Integer.parseInt(fieldAge.getText().toString());
        int grade = Integer.parseInt(fieldGrade.getText().toString());

        kid.setAge(age);
        kid.setGrade(grade);
        kid.setName(name);
    }
}