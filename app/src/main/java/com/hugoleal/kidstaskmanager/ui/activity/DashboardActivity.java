package com.hugoleal.kidstaskmanager.ui.activity;

import android.app.DatePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.model.Kid;
import com.hugoleal.kidstaskmanager.ui.adapter.DashboardListAdapter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@RequiresApi(api = Build.VERSION_CODES.O)
public class DashboardActivity extends AppCompatActivity {

    private static final String ACTIVITY_TITLE = "Dashboard";
    private DashboardListAdapter dashboardListAdapter;
    private LocalDate firstDay = null;
    private LocalDate lastDay = null;
    private Kid kid;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_dashboard);
        setTitle(ACTIVITY_TITLE);
        configureKidId();
        backButtonConfigure();
        listConfigure();

        //tasksListActivityHelper = new TasksListActivityHelper(this, tasksListAdapter, taskDAO);
    }

    private void configureKidId() {

        kid = (Kid) getIntent().getSerializableExtra("kid");
    }

    @Override
    public void onResume() {
        super.onResume();
        setPeriod();
        dashboardListAdapter.updateList(firstDay, lastDay);
    }

    private void setPeriod() {

        if (lastDay == null) {
            lastDay = LocalDate.now().minusDays(1);
        }
        if (firstDay == null) {
            DayOfWeek dayOfWeek = lastDay.getDayOfWeek();
            firstDay = lastDay.minusDays(dayOfWeek.getValue() - 1);
        }
        configurePeriodButtons();
    }

    private void configurePeriodButtons() {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getString(R.string.dateFormat));

        Button initButton = findViewById(R.id.activity_dashboard_first_day_button);
        initButton.setText(firstDay.format(formatter));
        initButton.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(DashboardActivity.this);
            datePickerDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                firstDay = LocalDate.of(year, month + 1, dayOfMonth);
                if (firstDay.compareTo(lastDay) > 0){
                    lastDay = firstDay;
                }
                onResume();
            });
            datePickerDialog.show();
        });

        Button lastButton = findViewById(R.id.activity_dashboard_last_day_button);
        lastButton.setText(lastDay.format(formatter));
        lastButton.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(DashboardActivity.this);
            datePickerDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                lastDay = LocalDate.of(year, month + 1, dayOfMonth);
                if (lastDay.compareTo(firstDay) < 0){
                    firstDay = lastDay;
                }
                onResume();
            });
            datePickerDialog.show();
        });
    }


    private void backButtonConfigure() {
        Button backButton = findViewById(R.id.activity_dashboard_back);
        backButton.setOnClickListener(v -> finish());
    }

    private void listConfigure() {
        ListView dashboardItemList = findViewById(R.id.activity_dashboard_list);
        dashboardListAdapter = new DashboardListAdapter(this, kid);
        dashboardItemList.setAdapter(dashboardListAdapter);
    }
}
