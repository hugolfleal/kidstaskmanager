package com.hugoleal.kidstaskmanager.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hugoleal.kidstaskmanager.R;
import com.hugoleal.kidstaskmanager.database.TasksDatabase;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Task;

import java.util.ArrayList;
import java.util.List;

public class EffectiveTaskListAdapter extends BaseAdapter {

    private final List<EffectiveTask> effectiveTasks = new ArrayList<>();
    private final Context context;

    public EffectiveTaskListAdapter(Context context) {
        this.context = context;
    }


    @Override
    public int getCount() {
        return effectiveTasks.size();
    }

    @Override
    public EffectiveTask getItem(int position) {
        return effectiveTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return effectiveTasks.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View newView = LayoutInflater.from(context).inflate(R.layout.item_effective_task, parent, false);
        EffectiveTask effectiveTask = effectiveTasks.get(position);
        int id = effectiveTask.getTaskId();
        TextView effectiveTaskName = newView.findViewById(R.id.item_effective_task_name);
        System.out.println(id);
        Task task = TasksDatabase.getInstance(context)
                .getTaskDAO()
                .getById(id);
        System.out.println(task.getName());
        effectiveTaskName.setText(task.getName());

        TextView effectiveTaskRate = newView.findViewById(R.id.item_effective_task_rate);
        effectiveTaskRate.setText(Integer.toString(effectiveTask.getRate()));

        return newView;
    }

    public void remove(EffectiveTask effectiveTask) {
        effectiveTasks.remove(effectiveTask);
        notifyDataSetChanged();
    }

    public void clear() {
        effectiveTasks.clear();
    }

    public void addAll(List<EffectiveTask> effectiveTask) {
        this.effectiveTasks.addAll(effectiveTask);
        notifyDataSetChanged();
    }
}
