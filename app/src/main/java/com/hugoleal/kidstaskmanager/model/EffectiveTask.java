package com.hugoleal.kidstaskmanager.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class EffectiveTask implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int kidId;
    private LocalDate date;
    private int taskId;
    private int rate;

    public EffectiveTask(int kidId, LocalDate date, int taskId) {
        this.kidId = kidId;
        this.date = date;
        this.taskId = taskId;
        this.rate = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKidId() {
        return kidId;
    }

    public void setKidId(int kidId) {
        this.kidId = kidId;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
