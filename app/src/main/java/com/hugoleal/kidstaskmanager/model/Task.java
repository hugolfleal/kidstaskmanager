package com.hugoleal.kidstaskmanager.model;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;
import java.time.LocalDate;

@Entity
public class Task implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;
    private Frequency frequency;
    private int importance;
    private String description;
    private boolean isDaily;
    private String dayOfWeek;
    private int dayOfMonth;
    private LocalDate date;
    private int kidId;

    public Task(LocalDate date, int kidId) {
        this.date = date;
        this.kidId = kidId;
    }

    public int getKidId() {
        return kidId;
    }

    public void setKidId(int kidId) {
        this.kidId = kidId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Frequency getFrequency() {
        return frequency;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setFrequency(Frequency frequency) {
        this.frequency = frequency;
        cleaningFrequency();
        setIsDaily(frequency == Frequency.DAILY);
        if (frequency == Frequency.WEEKLY){
            setDayOfWeek(date.getDayOfWeek().toString());
        } else if (frequency == Frequency.MONTHLY){
            setDayOfMonth(date.getDayOfMonth());
        }
    }

    private void cleaningFrequency() {
        setIsDaily(false);
        setDayOfWeek("");
        setDayOfMonth(0);
    }

    public int getImportance() {
        return importance;
    }

    public void setImportance(int importance) {
        this.importance = importance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasId() {
        return id > 0;
    }

    public boolean isDaily() {
        return isDaily;
    }

    public void setIsDaily(boolean isDaily) {
        this.isDaily = isDaily;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;

    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
