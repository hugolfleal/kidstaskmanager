package com.hugoleal.kidstaskmanager.model;

import com.hugoleal.kidstaskmanager.R;

public enum Importance {
    NONE(0, R.id.activity_add_task_no_importance_radioButton),
    LOW(5, R.id.activity_add_task_low_radioButton),
    MEDIUM(10, R.id.activity_add_task_medium_radioButton),
    HIGH(15, R.id.activity_add_task_high_radioButton);

    private int imp;
    private int radioButtonId;

    Importance(int imp, int radioButtonId){
        this.imp = imp;
        this.radioButtonId = radioButtonId;
    };

   /*
   public static int getImp(Importance importance) {
        return importance.imp;
    }
    */

    public static int getButtonId(int importance) {
        for (Importance i : Importance.values()) {
            if (importance == i.getImp()){
                return i.getRadioButtonId();
            }
        }
        return Importance.NONE.radioButtonId;
    }

    public int getImp() {
        return imp;
    }

    public int getRadioButtonId() {
        return radioButtonId;
    }
}
