package com.hugoleal.kidstaskmanager.model;

import com.hugoleal.kidstaskmanager.R;

public enum Frequency {
    NONE("NONE", R.id.activity_add_task_unrepeated_radioButton),
    DAILY("DAILY", R.id.activity_add_task_daily_radioButton),
    WEEKLY("WEEKLY", R.id.activity_add_task_weekly_radioButton),
    MONTHLY("MONTHLY", R.id.activity_add_task_monthly_radioButton);


    private String str;
    private int radioButtonId;

    Frequency(String str, int radioButtonId){
        this.str = str;
        this.radioButtonId = radioButtonId;
    };

    public String getStr() {
        return str;
    }

    public int getButtonId() {
        return radioButtonId;
    }
}
