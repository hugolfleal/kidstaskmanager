package com.hugoleal.kidstaskmanager.model;

import java.time.LocalDate;

public class DashboardItem {

    private LocalDate date;
    private double dayRate;

    public DashboardItem(LocalDate date, double dayRate) {
        this.date = date;
        this.dayRate = dayRate;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getDayRate() {
        return dayRate;
    }

    public void setDayRate(int dayRate) {
        this.dayRate = dayRate;
    }
}
