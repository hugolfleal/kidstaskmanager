package com.hugoleal.kidstaskmanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.hugoleal.kidstaskmanager.database.converter.FrequencyConverter;
import com.hugoleal.kidstaskmanager.database.converter.LocalDateConverter;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.Task;

@Database(entities = {Task.class}, version = 4, exportSchema = false)
@TypeConverters({LocalDateConverter.class, FrequencyConverter.class})
public abstract class TasksDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "Task.db";
    private static TasksDatabase instance;

    public abstract TaskDAO getTaskDAO();

    public static TasksDatabase getInstance(Context context){

        if (instance == null) {
            instance = Room.databaseBuilder(context, TasksDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }
}


// TODO: 24/03/2021 apagar o fallback quando nao for mais alterar o model task