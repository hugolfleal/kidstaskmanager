package com.hugoleal.kidstaskmanager.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.hugoleal.kidstaskmanager.model.Kid;

import java.util.List;

@Dao
public interface KidDAO {

    @Insert
    void save (Kid kid);

    @Query("SELECT * FROM kid")
    List<Kid> all();

    @Delete
    void remove(Kid kid);

    @Update
    void edit(Kid kid);
}
