package com.hugoleal.kidstaskmanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.hugoleal.kidstaskmanager.database.dao.KidDAO;
import com.hugoleal.kidstaskmanager.model.Kid;

@Database(entities = {Kid.class}, version = 1, exportSchema = false)
public abstract class KidsDatabase extends RoomDatabase {

    private static final String DATABASE_NAME = "kidsTaskManager.db";
    private static KidsDatabase instance;

    public abstract KidDAO getRoomKidDAO();

    public static KidsDatabase getInstance(Context context){

        if (instance == null) {
            instance = Room.databaseBuilder(context, KidsDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }
}
