package com.hugoleal.kidstaskmanager.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.hugoleal.kidstaskmanager.database.converter.LocalDateConverter;
import com.hugoleal.kidstaskmanager.database.dao.EffectiveTaskDAO;
import com.hugoleal.kidstaskmanager.database.dao.TaskDAO;
import com.hugoleal.kidstaskmanager.model.EffectiveTask;

@Database(entities = {EffectiveTask.class}, version = 3, exportSchema = false)
@TypeConverters({LocalDateConverter.class})
public abstract class EffectiveTaskDatabase extends RoomDatabase{

        private static final String DATABASE_NAME = "EffectiveTask.db";
        private static EffectiveTaskDatabase instance;

        public abstract EffectiveTaskDAO getEffectiveTaskDAO();

        public static EffectiveTaskDatabase getInstance(Context context){

            if (instance == null) {
                instance = Room.databaseBuilder(context, EffectiveTaskDatabase.class, DATABASE_NAME)
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .build();
            }
            return instance;
        }

}
