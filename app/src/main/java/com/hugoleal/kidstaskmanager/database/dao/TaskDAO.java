package com.hugoleal.kidstaskmanager.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.hugoleal.kidstaskmanager.model.Task;

import java.time.LocalDate;
import java.util.List;

@Dao
public interface TaskDAO {

    @Insert
    void save (Task task);

    @Query("SELECT * FROM task")
    List<Task> all();

    @Query("SELECT * FROM task WHERE kidId IN(:kidId)")
    List<Task> getByKidId (int kidId);

    @Delete
    void remove(Task task);

    @Update
    void edit(Task task);

    @Query("SELECT * FROM task WHERE id IN(:id)")
    Task getById(int id);

    @Query("SELECT * FROM task WHERE kidId IN(:kidId) AND isDaily = 1 OR dayOfWeek IN(:dayOfWeek) OR dayOfMonth IN(:dayOfMonth)")
    List<Task> getByFrequency(String dayOfWeek, int dayOfMonth, int kidId);

    @Query("SELECT DISTINCT importance FROM task WHERE kidId IN (:kidId) AND id IN (:taskId)")
    int getImportanceBYTaskAndKidId (int kidId, int taskId);
}
