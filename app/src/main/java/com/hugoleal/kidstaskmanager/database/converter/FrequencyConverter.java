package com.hugoleal.kidstaskmanager.database.converter;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.TypeConverter;

import com.hugoleal.kidstaskmanager.model.Frequency;


public class FrequencyConverter {

        @RequiresApi(api = Build.VERSION_CODES.O)
        @TypeConverter
        public static Frequency toFrequency(String frequencyString) {
            if (frequencyString == null) {
                return null;
            } else {
                return Frequency.valueOf(frequencyString);
            }
        }

        @TypeConverter
        public static String toFrequencyString(Frequency frequency) {
            if (frequency == null) {
                return null;
            } else {
                return frequency.toString();
            }
        }


}
