package com.hugoleal.kidstaskmanager.database.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.hugoleal.kidstaskmanager.model.EffectiveTask;
import com.hugoleal.kidstaskmanager.model.Task;

import java.time.LocalDate;
import java.util.List;

@Dao
public interface EffectiveTaskDAO {


        @Insert
        void save (EffectiveTask effectiveTask);

        @Query("SELECT * FROM effectiveTask")
        List<EffectiveTask> all();

        @Delete
        void remove(EffectiveTask effectiveTask);

        @Update
        void edit(EffectiveTask effectiveTask);

        @Query("SELECT * FROM effectivetask WHERE date IN(:date) AND kidId IN(:kidId)")
        List<EffectiveTask> getByIdAndDate(LocalDate date, int kidId);

        @Query("SELECT * FROM effectivetask WHERE taskId IN(:taskId)")
        List<EffectiveTask> getEffectiveTaskByTaskId(Integer taskId);
}
